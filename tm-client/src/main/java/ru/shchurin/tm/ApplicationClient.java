package ru.shchurin.tm;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.BootstrapClient;

public final class ApplicationClient {

    public static void main(String[] args) throws Exception {
        @NotNull final BootstrapClient bootstrap = new BootstrapClient();
        bootstrap.init();
        bootstrap.start();
    }
}
