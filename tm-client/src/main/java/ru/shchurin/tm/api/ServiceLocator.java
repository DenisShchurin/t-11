package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.endpoint.DomainEndpoint;
import ru.shchurin.tm.endpoint.ProjectEndpoint;
import ru.shchurin.tm.endpoint.TaskEndpoint;
import ru.shchurin.tm.endpoint.UserEndpoint;

public interface ServiceLocator {

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    DomainEndpoint getDomainEndpoint();
}
