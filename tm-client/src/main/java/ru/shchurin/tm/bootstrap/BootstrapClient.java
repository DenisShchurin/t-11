package ru.shchurin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.*;
import ru.shchurin.tm.exeption.CommandCorruptException;
import ru.shchurin.tm.exeption.UserHasNotAccess;
import ru.shchurin.tm.exeption.UserNotAuthorized;
import ru.shchurin.tm.util.ConsoleUtil;

import java.lang.Exception;
import java.util.*;

@NotNull
public class BootstrapClient implements ServiceLocator {

    private static final String GREETING = "*** WELCOME TO TASK MANAGER ***";

    @NotNull
    private static final String EXIT = "exit";

    @NotNull
    private final Map<String, AbstractCommand> commands = new HashMap<>();

    @Nullable
    private Session session;

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointImplService().getProjectEndpointImplPort();

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointImplService().getTaskEndpointImplPort();

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointImplService().getUserEndpointImplPort();

    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpointImplService().getDomainEndpointImplPort();

    @Nullable
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.shchurin.tm").getSubTypesOf(AbstractCommand.class);


    private void register(@Nullable final Class clazz) throws Exception {
        if (clazz == null) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();

        @Nullable final String cliCommand = command.getCommand();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    public void init() throws Exception {
        if (classes == null) return;
        for (@Nullable final Class clazz : classes)
            register(clazz);
    }

    public void start() throws Exception {
        System.out.println(GREETING);
        @NotNull String command = "";
        while (!EXIT.equals(command)) {
            command = ConsoleUtil.getStringFromConsole();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;

        final boolean commandIsNotSafe = !abstractCommand.isSafe();
        if (commandIsNotSafe && session == null)
            throw new UserNotAuthorized();

        final boolean userAuthorizedAndHasNotAccessRight = session != null &&
                !abstractCommand.getRoles().contains(session.getRole());
        if (commandIsNotSafe && userAuthorizedAndHasNotAccessRight) throw new UserHasNotAccess();
        abstractCommand.execute();
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public @NotNull ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    public @NotNull TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    public @NotNull UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @Override
    public @NotNull DomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    @Nullable
    public User getSession() {
        return session;
    }

    public void setSession(@Nullable final Session session) {
        this.session = session;
    }
}
