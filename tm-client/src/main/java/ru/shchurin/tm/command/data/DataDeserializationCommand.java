package ru.shchurin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import ru.shchurin.tm.enumerated.Role;

public final class DataDeserializationCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "d-d";

    @NotNull
    private static final String DESCRIPTION = "Deserialize application data.";

    @NotNull
    private static final String DESERIALIZE = "[DESERIALIZE APPLICATION DATA]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(DESERIALIZE);
        serviceLocator.getDomainEndpoint().dataDeserialize();
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
