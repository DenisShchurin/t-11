package ru.shchurin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.BootstrapClient;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class DataSerializationCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "d-s";

    @NotNull
    private static final String DESCRIPTION = "Serialize application data.";

    @NotNull
    private static final String SERIALIZE = "[SERIALIZE APPLICATION DATA ]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(SERIALIZE);
        @Nullable final Session session = ((BootstrapClient) serviceLocator).getSession();
        serviceLocator.getDomainEndpoint().dataSerialize(session);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
