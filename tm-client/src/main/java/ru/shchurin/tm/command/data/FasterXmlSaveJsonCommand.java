package ru.shchurin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class FasterXmlSaveJsonCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "f-s-j";

    @NotNull
    private static final String DESCRIPTION = "Save application data.";

    @NotNull
    private static final String SAVING = "[SAVE APPLICATION DATA]";

    @NotNull
    private static final String FILE = "src/main/data/fasterxml.json";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(SAVING);
        serviceLocator.getDomainEndpoint().fasterXmlSaveJson();
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
