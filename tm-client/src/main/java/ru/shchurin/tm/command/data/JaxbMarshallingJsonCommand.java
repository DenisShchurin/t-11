package ru.shchurin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class JaxbMarshallingJsonCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "j-m-j";

    @NotNull
    private static final String DESCRIPTION = "Marshalling application data.";

    @NotNull
    private static final String MARSHALLING = "[MARSHALLING APPLICATION DATA ]";

    @NotNull
    private static final String FILE = "src/main/data/jaxb.json";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(MARSHALLING);
        serviceLocator.getDomainEndpoint().jaxbMarshallingJson();
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
