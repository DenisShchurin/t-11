package ru.shchurin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class JaxbUnmarshallingXmlCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "j-u";

    @NotNull
    private static final String DESCRIPTION = "Unmarshalling application data.";

    @NotNull
    private static final String UNMARSHALLING = "[UNMARSHALLING APPLICATION DATA ]";

    @NotNull
    private static final String FILE = "src/main/data/jaxb.xml";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(UNMARSHALLING);
        serviceLocator.getDomainEndpoint().jaxbUnmarshallingXml();
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
