package ru.shchurin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.BootstrapClient;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "p-c";

    @NotNull
    private static final String DESCRIPTION = "Create new project.";

    @NotNull
    private static final String PROJECT_CREATE = "[PROJECT CREATE]";

    @NotNull
    private static final String PROJECT_CREATED = "[PROJECT CREATED]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(PROJECT_CREATE);
        System.out.println(ENTER_NAME);
        @Nullable final String name = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_DESCRIPTION);
        @Nullable final String description = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_START_DATE);
        @Nullable final String dateStart = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_END_DATE);
        @Nullable final String dateFinish = ConsoleUtil.getStringFromConsole();
        @Nullable final Session session = ((BootstrapClient) serviceLocator).getSession();
        serviceLocator.getProjectEndpoint().persistProject(session, name, description, dateStart, dateFinish);
        System.out.println(PROJECT_CREATED);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}

