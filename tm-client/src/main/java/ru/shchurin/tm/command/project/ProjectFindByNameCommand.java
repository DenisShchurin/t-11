package ru.shchurin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.BootstrapClient;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Project;
import ru.shchurin.tm.endpoint.Role;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.util.DateUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ProjectFindByNameCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "project-find-by-name";

    @NotNull
    private static final String DESCRIPTION = "Find project by part of the name.";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(FIND_PROJECT);
        System.out.println(ENTER_NAME);
        @Nullable final String name = ConsoleUtil.getStringFromConsole();
        @Nullable final Session session = ((BootstrapClient) serviceLocator).getSession();
        int index = 1;
        @Nullable final List<Project> projects = serviceLocator.getProjectEndpoint().findByNameProject(session, name);
        if (projects == null) return;
        for (@NotNull final Project project : projects) {
            System.out.println(index++ + ". " +
                    "Project{" +
                    "ID='" + project.getId() + '\'' +
                    "project name='" + project.getName() + '\'' +
                    ", description='" + project.getDescription() + '\'' +
                    ", dateStart=" + DateUtil.dateFormat(project.getDateStart().toGregorianCalendar().getTime()) +
                    ", dateFinish=" + DateUtil.dateFormat(project.getDateFinish().toGregorianCalendar().getTime()) +
                    ", userId='" + project.getUserId() + '\'' +
                    ", status=" + project.getStatus() +
                    ", dateCreation=" + DateUtil.dateFormat(project.getDateCreation().toGregorianCalendar().getTime()) +
                    '}');
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
