package ru.shchurin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.BootstrapClient;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Project;
import ru.shchurin.tm.endpoint.Role;
import ru.shchurin.tm.endpoint.User;
import ru.shchurin.tm.util.DateUtil;

import java.util.*;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "p-l";

    @NotNull
    private static final String DESCRIPTION = "Show all projects.";

    @NotNull
    private static final String PROJECT_LIST = "[PROJECT LIST]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = ((BootstrapClient) serviceLocator).getSession();
        System.out.println(PROJECT_LIST);
        int index = 1;
        @Nullable final List<Project> projects = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(session));
        if (projects == null) return;
        for (@NotNull final Project project : projects) {
            System.out.println(index++ + ". " +
                    "Project{" +
                    "ID='" + project.getId() + '\'' +
                    "project name='" + project.getName() + '\'' +
                    ", description='" + project.getDescription() + '\'' +
                    ", dateStart=" + DateUtil.dateFormat(project.getDateStart().toGregorianCalendar().getTime()) +
                    ", dateFinish=" + DateUtil.dateFormat(project.getDateFinish().toGregorianCalendar().getTime()) +
                    ", userId='" + project.getUserId() + '\'' +
                    ", status=" + project.getStatus() +
                    ", dateCreation=" + DateUtil.dateFormat(project.getDateCreation().toGregorianCalendar().getTime()) +
                    '}');
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
