package ru.shchurin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.BootstrapClient;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "project-remove";

    @NotNull
    private static final String DESCRIPTION = "Remove selected project and his tasks.";

    @NotNull
    private static final String REMOVED = "[PROJECT AND HIS TASKS REMOVED]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(ENTER_PROJECT_NAME);
        @Nullable final String name = ConsoleUtil.getStringFromConsole();
        @Nullable final Session session = ((BootstrapClient) serviceLocator).getSession();
        serviceLocator.getProjectEndpoint().removeProjectAndTasksByName(session, name);
        System.out.println(REMOVED);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
