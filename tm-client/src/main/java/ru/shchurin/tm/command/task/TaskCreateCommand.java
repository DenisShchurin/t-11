package ru.shchurin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.BootstrapClient;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "t-c";

    @NotNull
    private static final String DESCRIPTION = "Create new task.";

    @NotNull
    private static final String TASK_CREATE = "[TASK CREATE]";

    @NotNull
    private static final String TASK_CREATED = "[TASK CREATED]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(TASK_CREATE);
        System.out.println(ENTER_NAME);
        @Nullable final String name = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_DESCRIPTION);
        @Nullable final String description = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_START_DATE);
        @Nullable final String dateStart = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_END_DATE);
        @Nullable final String dateFinish = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_PROJECT_ID);
        @Nullable final String projectId = ConsoleUtil.getStringFromConsole();
        @Nullable final Session session = ((BootstrapClient) serviceLocator).getSession();
        serviceLocator.getTaskEndpoint().persistTask(session, name, description, projectId, dateStart, dateFinish);
        System.out.println(TASK_CREATED);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
