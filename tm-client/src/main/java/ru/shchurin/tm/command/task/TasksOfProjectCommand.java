package ru.shchurin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.BootstrapClient;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;
import ru.shchurin.tm.endpoint.Task;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.util.DateUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TasksOfProjectCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "tasks-of-project";

    @NotNull
    private static final String DESCRIPTION = "Show all tasks of project.";

    @NotNull
    private static final String TASKS_OF_PROJECT = "[TASKS OF PROJECT]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(TASKS_OF_PROJECT);
        System.out.println(ENTER_PROJECT_NAME);
        @Nullable final String name = ConsoleUtil.getStringFromConsole();
        @Nullable final Session session = ((BootstrapClient) serviceLocator).getSession();
        @Nullable final List<Task> tasks = serviceLocator.getProjectEndpoint().getTasksOfProjectByName(session, name);
        if (tasks == null) return;
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index++ + ". " +
                    "Task{" +
                    "ID='" + task.getId() + '\'' +
                    "task name='" + task.getName() + '\'' +
                    ", description='" + task.getDescription() + '\'' +
                    ", dateStart=" + DateUtil.dateFormat(task.getDateStart().toGregorianCalendar().getTime()) +
                    ", dateFinish=" + DateUtil.dateFormat(task.getDateFinish().toGregorianCalendar().getTime()) +
                    ", projectId='" + task.getProjectId() + '\'' +
                    ", userId='" + task.getUserId() + '\'' +
                    ", status=" + task.getStatus() +
                    ", dateCreation=" + DateUtil.dateFormat(task.getDateCreation().toGregorianCalendar().getTime()) +
                    '}');
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
