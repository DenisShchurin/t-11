package ru.shchurin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.BootstrapClient;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;
import ru.shchurin.tm.endpoint.User;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserAuthorizationCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "u-a";

    @NotNull
    private static final String DESCRIPTION = "Authorise user.";

    @NotNull
    private static final String USER_AUTHORIZATION = "[USER AUTHORIZATION]";

    @NotNull
    private static final String USER_NOT_FOUND = "[USER NOT FOUND]";

    @NotNull
    private static final String USER_AUTHORIZED = "[USER AUTHORIZED]";

    private final boolean safe = true;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(USER_AUTHORIZATION);
        System.out.println(ENTER_LOGIN);
        @Nullable final String login = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_PASSWORD);
        @Nullable final String password = ConsoleUtil.getStringFromConsole();
        @Nullable final Session session = serviceLocator.getUserEndpoint().getSession(login, password);
        if (session == null) {
            System.out.println(USER_NOT_FOUND);
        } else {
            ((BootstrapClient) serviceLocator).setSession(session);
            System.out.println(USER_AUTHORIZED);
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
