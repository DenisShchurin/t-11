package ru.shchurin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.BootstrapClient;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;
import ru.shchurin.tm.endpoint.User;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserEditProfileCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "user-edit-profile";

    @NotNull
    private static final String DESCRIPTION = "Edit user profile.";

    @NotNull
    private static final String EDIT_USER_PROFILE = "[EDIT USER PROFILE]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = ((BootstrapClient) serviceLocator).getSession();
        System.out.println(EDIT_USER_PROFILE);
        System.out.println(ENTER_LOGIN);
        @Nullable final String login = ConsoleUtil.getStringFromConsole();
        serviceLocator.getUserEndpoint().mergeUser(session, login);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
