package ru.shchurin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.BootstrapClient;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;
import ru.shchurin.tm.endpoint.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class UserListCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "u-l";

    @NotNull
    private static final String DESCRIPTION = "Show all users.";

    @NotNull
    private static final String USER_LIST = "[USER LIST]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_ADMIN));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println(USER_LIST);
        int index = 1;
        @Nullable final Session session = ((BootstrapClient) serviceLocator).getSession();
        @NotNull final List<User> users = serviceLocator.getUserEndpoint().findAllUsers(session);
        for (@NotNull final User user : users) {
            System.out.println(index++ + ". " +
                    "User{" +
                    "id='" + user.getId() + '\'' +
                    ", login='" + user.getLogin() + '\'' +
                    ", hashPassword='" + user.getHashPassword() + '\'' +
                    ", role=" + user.getRole() +
                    '}'
                    );
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
