package ru.shchurin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserRegistrationCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "user-registration";

    @NotNull
    private static final String DESCRIPTION = "Register user.";

    @NotNull
    private static final String USER_REGISTRATION = "[USER REGISTRATION]";

    @NotNull
    private static final String USER_REGISTERED = "[USER REGISTERED]";

    private final boolean safe = true;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(USER_REGISTRATION);
        System.out.println(ENTER_LOGIN);
        @Nullable final String login = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_PASSWORD);
        @Nullable final String password = ConsoleUtil.getStringFromConsole();
        serviceLocator.getUserEndpoint().persistUser(login, password);
        System.out.println(USER_REGISTERED);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
