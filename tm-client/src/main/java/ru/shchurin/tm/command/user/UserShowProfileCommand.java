package ru.shchurin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.BootstrapClient;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.endpoint.Role;
import ru.shchurin.tm.endpoint.User;

import java.util.ArrayList;
import java.util.List;

public final class UserShowProfileCommand extends AbstractCommand {

    @NotNull
    private static final String COMMAND = "user-show-profile";

    @NotNull
    private static final String DESCRIPTION = "Show user profile.";

    @NotNull
    private static final String SHOW_USER_PROFILE = "[SHOW USER PROFILE]";

    private final boolean safe = false;

    @NotNull
    private final ArrayList<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println(SHOW_USER_PROFILE);
        @Nullable final Session session = ((BootstrapClient) serviceLocator).getSession();
        @NotNull final User user = serviceLocator.getUserEndpoint().findOneUser(session);
        System.out.println(user.getLogin() + ", " + user.getRole());
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
