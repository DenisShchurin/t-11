
package ru.shchurin.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.shchurin.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "Exception");
    private final static QName _DataDeserialize_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "dataDeserialize");
    private final static QName _DataDeserializeResponse_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "dataDeserializeResponse");
    private final static QName _DataSerialize_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "dataSerialize");
    private final static QName _DataSerializeResponse_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "dataSerializeResponse");
    private final static QName _FasterXmlLoadJson_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "fasterXmlLoadJson");
    private final static QName _FasterXmlLoadJsonResponse_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "fasterXmlLoadJsonResponse");
    private final static QName _FasterXmlLoadXml_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "fasterXmlLoadXml");
    private final static QName _FasterXmlLoadXmlResponse_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "fasterXmlLoadXmlResponse");
    private final static QName _FasterXmlSaveJson_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "fasterXmlSaveJson");
    private final static QName _FasterXmlSaveJsonResponse_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "fasterXmlSaveJsonResponse");
    private final static QName _FasterXmlSaveXml_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "fasterXmlSaveXml");
    private final static QName _FasterXmlSaveXmlResponse_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "fasterXmlSaveXmlResponse");
    private final static QName _JaxbMarshallingJson_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "jaxbMarshallingJson");
    private final static QName _JaxbMarshallingJsonResponse_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "jaxbMarshallingJsonResponse");
    private final static QName _JaxbMarshallingXml_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "jaxbMarshallingXml");
    private final static QName _JaxbMarshallingXmlResponse_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "jaxbMarshallingXmlResponse");
    private final static QName _JaxbUnmarshallingJson_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "jaxbUnmarshallingJson");
    private final static QName _JaxbUnmarshallingJsonResponse_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "jaxbUnmarshallingJsonResponse");
    private final static QName _JaxbUnmarshallingXml_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "jaxbUnmarshallingXml");
    private final static QName _JaxbUnmarshallingXmlResponse_QNAME = new QName("http://endpoint.tm.shchurin.ru/", "jaxbUnmarshallingXmlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.shchurin.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link DataDeserialize }
     * 
     */
    public DataDeserialize createDataDeserialize() {
        return new DataDeserialize();
    }

    /**
     * Create an instance of {@link DataDeserializeResponse }
     * 
     */
    public DataDeserializeResponse createDataDeserializeResponse() {
        return new DataDeserializeResponse();
    }

    /**
     * Create an instance of {@link DataSerialize }
     * 
     */
    public DataSerialize createDataSerialize() {
        return new DataSerialize();
    }

    /**
     * Create an instance of {@link DataSerializeResponse }
     * 
     */
    public DataSerializeResponse createDataSerializeResponse() {
        return new DataSerializeResponse();
    }

    /**
     * Create an instance of {@link FasterXmlLoadJson }
     * 
     */
    public FasterXmlLoadJson createFasterXmlLoadJson() {
        return new FasterXmlLoadJson();
    }

    /**
     * Create an instance of {@link FasterXmlLoadJsonResponse }
     * 
     */
    public FasterXmlLoadJsonResponse createFasterXmlLoadJsonResponse() {
        return new FasterXmlLoadJsonResponse();
    }

    /**
     * Create an instance of {@link FasterXmlLoadXml }
     * 
     */
    public FasterXmlLoadXml createFasterXmlLoadXml() {
        return new FasterXmlLoadXml();
    }

    /**
     * Create an instance of {@link FasterXmlLoadXmlResponse }
     * 
     */
    public FasterXmlLoadXmlResponse createFasterXmlLoadXmlResponse() {
        return new FasterXmlLoadXmlResponse();
    }

    /**
     * Create an instance of {@link FasterXmlSaveJson }
     * 
     */
    public FasterXmlSaveJson createFasterXmlSaveJson() {
        return new FasterXmlSaveJson();
    }

    /**
     * Create an instance of {@link FasterXmlSaveJsonResponse }
     * 
     */
    public FasterXmlSaveJsonResponse createFasterXmlSaveJsonResponse() {
        return new FasterXmlSaveJsonResponse();
    }

    /**
     * Create an instance of {@link FasterXmlSaveXml }
     * 
     */
    public FasterXmlSaveXml createFasterXmlSaveXml() {
        return new FasterXmlSaveXml();
    }

    /**
     * Create an instance of {@link FasterXmlSaveXmlResponse }
     * 
     */
    public FasterXmlSaveXmlResponse createFasterXmlSaveXmlResponse() {
        return new FasterXmlSaveXmlResponse();
    }

    /**
     * Create an instance of {@link JaxbMarshallingJson }
     * 
     */
    public JaxbMarshallingJson createJaxbMarshallingJson() {
        return new JaxbMarshallingJson();
    }

    /**
     * Create an instance of {@link JaxbMarshallingJsonResponse }
     * 
     */
    public JaxbMarshallingJsonResponse createJaxbMarshallingJsonResponse() {
        return new JaxbMarshallingJsonResponse();
    }

    /**
     * Create an instance of {@link JaxbMarshallingXml }
     * 
     */
    public JaxbMarshallingXml createJaxbMarshallingXml() {
        return new JaxbMarshallingXml();
    }

    /**
     * Create an instance of {@link JaxbMarshallingXmlResponse }
     * 
     */
    public JaxbMarshallingXmlResponse createJaxbMarshallingXmlResponse() {
        return new JaxbMarshallingXmlResponse();
    }

    /**
     * Create an instance of {@link JaxbUnmarshallingJson }
     * 
     */
    public JaxbUnmarshallingJson createJaxbUnmarshallingJson() {
        return new JaxbUnmarshallingJson();
    }

    /**
     * Create an instance of {@link JaxbUnmarshallingJsonResponse }
     * 
     */
    public JaxbUnmarshallingJsonResponse createJaxbUnmarshallingJsonResponse() {
        return new JaxbUnmarshallingJsonResponse();
    }

    /**
     * Create an instance of {@link JaxbUnmarshallingXml }
     * 
     */
    public JaxbUnmarshallingXml createJaxbUnmarshallingXml() {
        return new JaxbUnmarshallingXml();
    }

    /**
     * Create an instance of {@link JaxbUnmarshallingXmlResponse }
     * 
     */
    public JaxbUnmarshallingXmlResponse createJaxbUnmarshallingXmlResponse() {
        return new JaxbUnmarshallingXmlResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataDeserialize }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "dataDeserialize")
    public JAXBElement<DataDeserialize> createDataDeserialize(DataDeserialize value) {
        return new JAXBElement<DataDeserialize>(_DataDeserialize_QNAME, DataDeserialize.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataDeserializeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "dataDeserializeResponse")
    public JAXBElement<DataDeserializeResponse> createDataDeserializeResponse(DataDeserializeResponse value) {
        return new JAXBElement<DataDeserializeResponse>(_DataDeserializeResponse_QNAME, DataDeserializeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSerialize }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "dataSerialize")
    public JAXBElement<DataSerialize> createDataSerialize(DataSerialize value) {
        return new JAXBElement<DataSerialize>(_DataSerialize_QNAME, DataSerialize.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSerializeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "dataSerializeResponse")
    public JAXBElement<DataSerializeResponse> createDataSerializeResponse(DataSerializeResponse value) {
        return new JAXBElement<DataSerializeResponse>(_DataSerializeResponse_QNAME, DataSerializeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlLoadJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "fasterXmlLoadJson")
    public JAXBElement<FasterXmlLoadJson> createFasterXmlLoadJson(FasterXmlLoadJson value) {
        return new JAXBElement<FasterXmlLoadJson>(_FasterXmlLoadJson_QNAME, FasterXmlLoadJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlLoadJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "fasterXmlLoadJsonResponse")
    public JAXBElement<FasterXmlLoadJsonResponse> createFasterXmlLoadJsonResponse(FasterXmlLoadJsonResponse value) {
        return new JAXBElement<FasterXmlLoadJsonResponse>(_FasterXmlLoadJsonResponse_QNAME, FasterXmlLoadJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlLoadXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "fasterXmlLoadXml")
    public JAXBElement<FasterXmlLoadXml> createFasterXmlLoadXml(FasterXmlLoadXml value) {
        return new JAXBElement<FasterXmlLoadXml>(_FasterXmlLoadXml_QNAME, FasterXmlLoadXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlLoadXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "fasterXmlLoadXmlResponse")
    public JAXBElement<FasterXmlLoadXmlResponse> createFasterXmlLoadXmlResponse(FasterXmlLoadXmlResponse value) {
        return new JAXBElement<FasterXmlLoadXmlResponse>(_FasterXmlLoadXmlResponse_QNAME, FasterXmlLoadXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlSaveJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "fasterXmlSaveJson")
    public JAXBElement<FasterXmlSaveJson> createFasterXmlSaveJson(FasterXmlSaveJson value) {
        return new JAXBElement<FasterXmlSaveJson>(_FasterXmlSaveJson_QNAME, FasterXmlSaveJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlSaveJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "fasterXmlSaveJsonResponse")
    public JAXBElement<FasterXmlSaveJsonResponse> createFasterXmlSaveJsonResponse(FasterXmlSaveJsonResponse value) {
        return new JAXBElement<FasterXmlSaveJsonResponse>(_FasterXmlSaveJsonResponse_QNAME, FasterXmlSaveJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlSaveXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "fasterXmlSaveXml")
    public JAXBElement<FasterXmlSaveXml> createFasterXmlSaveXml(FasterXmlSaveXml value) {
        return new JAXBElement<FasterXmlSaveXml>(_FasterXmlSaveXml_QNAME, FasterXmlSaveXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlSaveXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "fasterXmlSaveXmlResponse")
    public JAXBElement<FasterXmlSaveXmlResponse> createFasterXmlSaveXmlResponse(FasterXmlSaveXmlResponse value) {
        return new JAXBElement<FasterXmlSaveXmlResponse>(_FasterXmlSaveXmlResponse_QNAME, FasterXmlSaveXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxbMarshallingJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "jaxbMarshallingJson")
    public JAXBElement<JaxbMarshallingJson> createJaxbMarshallingJson(JaxbMarshallingJson value) {
        return new JAXBElement<JaxbMarshallingJson>(_JaxbMarshallingJson_QNAME, JaxbMarshallingJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxbMarshallingJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "jaxbMarshallingJsonResponse")
    public JAXBElement<JaxbMarshallingJsonResponse> createJaxbMarshallingJsonResponse(JaxbMarshallingJsonResponse value) {
        return new JAXBElement<JaxbMarshallingJsonResponse>(_JaxbMarshallingJsonResponse_QNAME, JaxbMarshallingJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxbMarshallingXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "jaxbMarshallingXml")
    public JAXBElement<JaxbMarshallingXml> createJaxbMarshallingXml(JaxbMarshallingXml value) {
        return new JAXBElement<JaxbMarshallingXml>(_JaxbMarshallingXml_QNAME, JaxbMarshallingXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxbMarshallingXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "jaxbMarshallingXmlResponse")
    public JAXBElement<JaxbMarshallingXmlResponse> createJaxbMarshallingXmlResponse(JaxbMarshallingXmlResponse value) {
        return new JAXBElement<JaxbMarshallingXmlResponse>(_JaxbMarshallingXmlResponse_QNAME, JaxbMarshallingXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxbUnmarshallingJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "jaxbUnmarshallingJson")
    public JAXBElement<JaxbUnmarshallingJson> createJaxbUnmarshallingJson(JaxbUnmarshallingJson value) {
        return new JAXBElement<JaxbUnmarshallingJson>(_JaxbUnmarshallingJson_QNAME, JaxbUnmarshallingJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxbUnmarshallingJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "jaxbUnmarshallingJsonResponse")
    public JAXBElement<JaxbUnmarshallingJsonResponse> createJaxbUnmarshallingJsonResponse(JaxbUnmarshallingJsonResponse value) {
        return new JAXBElement<JaxbUnmarshallingJsonResponse>(_JaxbUnmarshallingJsonResponse_QNAME, JaxbUnmarshallingJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxbUnmarshallingXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "jaxbUnmarshallingXml")
    public JAXBElement<JaxbUnmarshallingXml> createJaxbUnmarshallingXml(JaxbUnmarshallingXml value) {
        return new JAXBElement<JaxbUnmarshallingXml>(_JaxbUnmarshallingXml_QNAME, JaxbUnmarshallingXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxbUnmarshallingXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shchurin.ru/", name = "jaxbUnmarshallingXmlResponse")
    public JAXBElement<JaxbUnmarshallingXmlResponse> createJaxbUnmarshallingXmlResponse(JaxbUnmarshallingXmlResponse value) {
        return new JAXBElement<JaxbUnmarshallingXmlResponse>(_JaxbUnmarshallingXmlResponse_QNAME, JaxbUnmarshallingXmlResponse.class, null, value);
    }

}
