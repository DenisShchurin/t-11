package ru.shchurin.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-03-20T13:54:28.444+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.shchurin.ru/", name = "TaskEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface TaskEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.shchurin.ru/TaskEndpoint/removeAllTasksRequest", output = "http://endpoint.tm.shchurin.ru/TaskEndpoint/removeAllTasksResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.shchurin.ru/TaskEndpoint/removeAllTasks/Fault/Exception")})
    @RequestWrapper(localName = "removeAllTasks", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.RemoveAllTasks")
    @ResponseWrapper(localName = "removeAllTasksResponse", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.RemoveAllTasksResponse")
    public void removeAllTasks(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findAllTasksRequest", output = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findAllTasksResponse")
    @RequestWrapper(localName = "findAllTasks", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.FindAllTasks")
    @ResponseWrapper(localName = "findAllTasksResponse", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.FindAllTasksResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.shchurin.tm.endpoint.Task> findAllTasks();

    @WebMethod
    @Action(input = "http://endpoint.tm.shchurin.ru/TaskEndpoint/removeTaskRequest", output = "http://endpoint.tm.shchurin.ru/TaskEndpoint/removeTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.shchurin.ru/TaskEndpoint/removeTask/Fault/Exception")})
    @RequestWrapper(localName = "removeTask", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.RemoveTask")
    @ResponseWrapper(localName = "removeTaskResponse", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.RemoveTaskResponse")
    public void removeTask(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.shchurin.ru/TaskEndpoint/removeByNameTaskRequest", output = "http://endpoint.tm.shchurin.ru/TaskEndpoint/removeByNameTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.shchurin.ru/TaskEndpoint/removeByNameTask/Fault/Exception")})
    @RequestWrapper(localName = "removeByNameTask", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.RemoveByNameTask")
    @ResponseWrapper(localName = "removeByNameTaskResponse", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.RemoveByNameTaskResponse")
    public void removeByNameTask(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findByDescriptionTaskRequest", output = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findByDescriptionTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findByDescriptionTask/Fault/Exception")})
    @RequestWrapper(localName = "findByDescriptionTask", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.FindByDescriptionTask")
    @ResponseWrapper(localName = "findByDescriptionTaskResponse", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.FindByDescriptionTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.shchurin.tm.endpoint.Task> findByDescriptionTask(
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description,
        @WebParam(name = "currentUserId", targetNamespace = "")
        java.lang.String currentUserId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findOneTaskRequest", output = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findOneTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findOneTask/Fault/Exception")})
    @RequestWrapper(localName = "findOneTask", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.FindOneTask")
    @ResponseWrapper(localName = "findOneTaskResponse", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.FindOneTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.shchurin.tm.endpoint.Task findOneTask(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.shchurin.ru/TaskEndpoint/persistTaskRequest", output = "http://endpoint.tm.shchurin.ru/TaskEndpoint/persistTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.shchurin.ru/TaskEndpoint/persistTask/Fault/Exception")})
    @RequestWrapper(localName = "persistTask", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.PersistTask")
    @ResponseWrapper(localName = "persistTaskResponse", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.PersistTaskResponse")
    public void persistTask(
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId,
        @WebParam(name = "dateStart", targetNamespace = "")
        java.lang.String dateStart,
        @WebParam(name = "dateFinish", targetNamespace = "")
        java.lang.String dateFinish,
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.shchurin.ru/TaskEndpoint/mergeTaskRequest", output = "http://endpoint.tm.shchurin.ru/TaskEndpoint/mergeTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.shchurin.ru/TaskEndpoint/mergeTask/Fault/Exception")})
    @RequestWrapper(localName = "mergeTask", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.MergeTask")
    @ResponseWrapper(localName = "mergeTaskResponse", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.MergeTaskResponse")
    public void mergeTask(
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId,
        @WebParam(name = "dateStart", targetNamespace = "")
        java.lang.String dateStart,
        @WebParam(name = "dateFinish", targetNamespace = "")
        java.lang.String dateFinish,
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "dateCreation", targetNamespace = "")
        java.lang.String dateCreation
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findAllTasksByUserIdRequest", output = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findAllTasksByUserIdResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findAllTasksByUserId/Fault/Exception")})
    @RequestWrapper(localName = "findAllTasksByUserId", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.FindAllTasksByUserId")
    @ResponseWrapper(localName = "findAllTasksByUserIdResponse", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.FindAllTasksByUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.shchurin.tm.endpoint.Task> findAllTasksByUserId(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findByNameTaskRequest", output = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findByNameTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.shchurin.ru/TaskEndpoint/findByNameTask/Fault/Exception")})
    @RequestWrapper(localName = "findByNameTask", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.FindByNameTask")
    @ResponseWrapper(localName = "findByNameTaskResponse", targetNamespace = "http://endpoint.tm.shchurin.ru/", className = "ru.shchurin.tm.endpoint.FindByNameTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.shchurin.tm.endpoint.Task> findByNameTask(
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "currentUserId", targetNamespace = "")
        java.lang.String currentUserId
    ) throws Exception_Exception;
}
