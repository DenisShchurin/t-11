package ru.shchurin.tm.endpoint;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-03-20T13:54:28.616+03:00
 * Generated source version: 3.2.7
 *
 */
@WebServiceClient(name = "UserEndpointImplService",
                  wsdlLocation = "http://localhost:8080/UserEndpointImpl?wsdl",
                  targetNamespace = "http://endpoint.tm.shchurin.ru/")
public class UserEndpointImplService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://endpoint.tm.shchurin.ru/", "UserEndpointImplService");
    public final static QName UserEndpointImplPort = new QName("http://endpoint.tm.shchurin.ru/", "UserEndpointImplPort");
    static {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/UserEndpointImpl?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(UserEndpointImplService.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "http://localhost:8080/UserEndpointImpl?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public UserEndpointImplService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public UserEndpointImplService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public UserEndpointImplService() {
        super(WSDL_LOCATION, SERVICE);
    }

    public UserEndpointImplService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public UserEndpointImplService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public UserEndpointImplService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns UserEndpoint
     */
    @WebEndpoint(name = "UserEndpointImplPort")
    public UserEndpoint getUserEndpointImplPort() {
        return super.getPort(UserEndpointImplPort, UserEndpoint.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns UserEndpoint
     */
    @WebEndpoint(name = "UserEndpointImplPort")
    public UserEndpoint getUserEndpointImplPort(WebServiceFeature... features) {
        return super.getPort(UserEndpointImplPort, UserEndpoint.class, features);
    }

}
