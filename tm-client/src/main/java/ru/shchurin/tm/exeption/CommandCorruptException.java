package ru.shchurin.tm.exeption;

public final class CommandCorruptException extends Exception {
    public CommandCorruptException() {
        super("DESCRIPTION MUST NOT BE NULL OR EMPTY");
    }
}
