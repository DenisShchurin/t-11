package ru.shchurin.tm.exeption;

public final class UserHasNotAccess extends Exception {
    public UserHasNotAccess() {
        super("You have not access");
    }
}
