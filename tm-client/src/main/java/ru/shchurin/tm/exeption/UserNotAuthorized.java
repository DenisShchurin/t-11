package ru.shchurin.tm.exeption;

public final class UserNotAuthorized extends Exception {
    public UserNotAuthorized() {
        super("You have not authorized");
    }
}
