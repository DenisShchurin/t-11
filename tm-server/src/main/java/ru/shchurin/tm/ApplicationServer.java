package ru.shchurin.tm;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.BootstrapServer;

public final class ApplicationServer {

    public static void main(String[] args) throws Exception {
        @NotNull final BootstrapServer bootstrapServer = new BootstrapServer();
        bootstrapServer.initUsers();
        bootstrapServer.registryEndpoint();
    }
}
