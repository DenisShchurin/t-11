package ru.shchurin.tm.api;

public interface DomainService {

    void dataSerialize() throws Exception;

    void dataDeserialize() throws Exception;

    void fasterXmlSaveXml() throws Exception;

    void fasterXmlLoadXml() throws Exception;

    void fasterXmlSaveJson() throws Exception;

    void fasterXmlLoadJson() throws Exception;

    void jaxbMarshallingXml() throws Exception;

    void jaxbUnmarshallingXml() throws Exception;

    void jaxbMarshallingJson() throws Exception;

    void jaxbUnmarshallingJson() throws Exception;
}
