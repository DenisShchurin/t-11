package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.*;
import ru.shchurin.tm.exception.*;

import java.util.Date;
import java.util.List;

public interface ProjectService {
    @NotNull
    List<Project> findAll(@Nullable String userId) throws Exception;

    @NotNull
    List<Project> findAll();

    @NotNull
    Project findOne(@Nullable String userId, @Nullable String id) throws Exception;


    void persist(
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String dateStart,
            @Nullable final String dateFinish,
            @Nullable final String userId
    ) throws Exception;

    void merge(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String dateStart,
            @Nullable final String dateFinish,
            @Nullable final String userId,
            @Nullable final String dateCreation
    ) throws Exception;

    void remove(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    void removeByName(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    List<Task> getTasksOfProject(@Nullable final String userId, @Nullable final String name) throws Exception;

    void removeProjectAndTasksByName(@Nullable final String userId, @Nullable final String name) throws Exception;

    @NotNull
    List<Project> findByName(@Nullable final String name, @NotNull final String currentUserId)
            throws Exception;

    @NotNull
    List<Project> findByDescription(@Nullable final String name, @NotNull final String currentUserId)
            throws Exception;
}
