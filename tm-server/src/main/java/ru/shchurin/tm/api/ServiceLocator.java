package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {
    @NotNull
    ProjectService getProjectService();

    @NotNull
    TaskService getTaskService();

    @NotNull
    UserService getUserService();

    @NotNull
    DomainService getDomainService();

    @NotNull
    SessionService getSessionService();
}
