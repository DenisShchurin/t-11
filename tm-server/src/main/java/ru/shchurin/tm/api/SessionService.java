package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.Session;

public interface SessionService {

    @Nullable
    Session getSession(@Nullable final String login, @Nullable final String password) throws Exception;

    boolean checkSession(@NotNull final Session session);
}
