package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.Task;

import java.util.Date;
import java.util.List;

public interface TaskService {
    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@Nullable String userId) throws Exception;

    @NotNull
    Task findOne(@Nullable String userId, @Nullable String id) throws Exception;

    void persist(
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String projectId,
            @Nullable final String dateStart,
            @Nullable final String dateFinish,
            @Nullable final String userId
    ) throws Exception;

    void merge(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String projectId,
            @Nullable final String dateStart,
            @Nullable final String dateFinish,
            @Nullable final String userId,
            @Nullable final String dateCreation
    ) throws Exception;

    void remove(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    void removeByName(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    List<Task> findByName(@Nullable final String name, @NotNull final String currentUserId)
            throws Exception;

    @NotNull
    List<Task> findByDescription(@Nullable final String description, @NotNull final String currentUserId)
            throws Exception;
}
