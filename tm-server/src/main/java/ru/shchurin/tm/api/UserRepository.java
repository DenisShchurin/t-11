package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.User;

import java.util.List;

public interface UserRepository extends Repository<User> {
    @NotNull
    List<User> findAll();

    @Nullable
    User findOne(@NotNull String id);

    void remove(@NotNull String id);

    void removeAll();

    void removeByLogin(@NotNull String login);

    boolean updatePassword(@NotNull final String userId, @NotNull final String newHashPassword);
}
