package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.Session;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.enumerated.Role;

import java.util.List;

public interface UserService {
    @NotNull
    List<User> findAll();

    @Nullable
    User findOne(@Nullable String id) throws Exception;

    void persist(@Nullable final String login,
                 @Nullable final String password) throws Exception;

    void merge(
            @Nullable final String userId,
            @Nullable final String login
            ) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void removeAll();

    void removeByLogin(@Nullable String login) throws Exception;

//    @Nullable
//    String getHashOfPassword(@Nullable String password) throws Exception;

    boolean updatePassword(@Nullable final Session session, @Nullable final String newPassword) throws Exception;

    void initUserAndAdmin() throws Exception;
}
