package ru.shchurin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.*;
import ru.shchurin.tm.endpoint.*;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.enumerated.Role;
import ru.shchurin.tm.repository.ProjectRepositoryImpl;
import ru.shchurin.tm.repository.TaskRepositoryImpl;
import ru.shchurin.tm.repository.UserRepositoryImpl;
import ru.shchurin.tm.service.*;

import javax.xml.ws.Endpoint;
import java.util.*;

public final class BootstrapServer implements ServiceLocator {

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepositoryImpl();

    @NotNull
    private final UserRepository userRepository = new UserRepositoryImpl();

    @NotNull
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository, taskRepository);

    @NotNull
    private final TaskService taskService = new TaskServiceImpl(taskRepository);

    @NotNull
    private final UserService userService = new UserServiceImpl(userRepository);

    @NotNull
    private final SessionService sessionService = new SessionServiceImpl(userRepository);

    @NotNull
    private final DomainService domainService = new DomainServiceImpl(projectService, taskService, userService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointImpl(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointImpl(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointImpl(this);

    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpointImpl(this);

    public void registryEndpoint() {
        Endpoint.publish("http://localhost:8080/ProjectEndpointImpl?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/TaskEndpointImpl?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/UserEndpointImpl?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/DomainEndpointImpl?wsdl", domainEndpoint);
    }

    public void initUsers() throws Exception {
        userService.initUserAndAdmin();
    }

    @NotNull
    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public UserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public DomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public SessionService getSessionService() {
        return sessionService;
    }
}
