package ru.shchurin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface DomainEndpoint {

    @WebMethod
    void dataSerialize(@Nullable final Session session) throws Exception;

    @WebMethod
    void dataDeserialize(@Nullable final Session session) throws Exception;

    @WebMethod
    void fasterXmlSaveXml(@Nullable final Session session) throws Exception;

    @WebMethod
    void fasterXmlLoadXml(@Nullable final Session session) throws Exception;

    @WebMethod
    void fasterXmlSaveJson(@Nullable final Session session) throws Exception;

    @WebMethod
    void fasterXmlLoadJson(@Nullable final Session session) throws Exception;

    @WebMethod
    void jaxbMarshallingXml(@Nullable final Session session) throws Exception;

    @WebMethod
    void jaxbUnmarshallingXml(@Nullable final Session session) throws Exception;

    @WebMethod
    void jaxbMarshallingJson(@Nullable final Session session) throws Exception;

    @WebMethod
    void jaxbUnmarshallingJson(@Nullable final Session session) throws Exception;
}
