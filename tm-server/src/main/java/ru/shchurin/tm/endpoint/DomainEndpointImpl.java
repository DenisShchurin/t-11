package ru.shchurin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.shchurin.tm.endpoint.DomainEndpoint")
public final class DomainEndpointImpl implements DomainEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    public DomainEndpointImpl(@Nullable ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Override
    public void dataSerialize(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getDomainService().dataSerialize();
    }

    @WebMethod
    @Override
    public void dataDeserialize(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getDomainService().dataDeserialize();
    }

    @WebMethod
    @Override
    public void fasterXmlSaveXml(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getDomainService().fasterXmlSaveXml();
    }

    @WebMethod
    @Override
    public void fasterXmlLoadXml(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getDomainService().fasterXmlLoadXml();
    }

    @WebMethod
    @Override
    public void fasterXmlSaveJson(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getDomainService().fasterXmlSaveJson();
    }

    @WebMethod
    @Override
    public void fasterXmlLoadJson(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getDomainService().fasterXmlLoadJson();
    }

    @WebMethod
    @Override
    public void jaxbMarshallingXml(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getDomainService().jaxbMarshallingXml();
    }

    @WebMethod
    @Override
    public void jaxbUnmarshallingXml(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getDomainService().jaxbUnmarshallingXml();
    }

    @WebMethod
    @Override
    public void jaxbMarshallingJson(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getDomainService().jaxbMarshallingJson();
    }

    @WebMethod
    @Override
    public void jaxbUnmarshallingJson(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getDomainService().jaxbUnmarshallingJson();
    }
}
