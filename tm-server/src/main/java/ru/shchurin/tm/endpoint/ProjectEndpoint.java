package ru.shchurin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Session;
import ru.shchurin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.Date;
import java.util.List;

@WebService
public interface ProjectEndpoint {

//    @NotNull
//    @WebMethod
//    List<Project> findAllProjects();

    @WebMethod
    @Nullable
    List<Project> findAllProjectsByUserId(@WebParam(name = "session") @Nullable final Session session) throws Exception;

//    @WebMethod
//    @NotNull
//    Project findOneProject(
//            @WebParam(name = "userId") @Nullable final String userId,
//            @WebParam(name = "id") @Nullable final String id) throws Exception;

    @WebMethod
    void persistProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "dateStart") @Nullable final String dateStart,
            @WebParam(name = "dateFinish") @Nullable final String dateFinish
    ) throws Exception;

    @WebMethod
    void mergeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "dateStart") @Nullable final String dateStart,
            @WebParam(name = "dateFinish") @Nullable final String dateFinish,
            @WebParam(name = "dateCreation") @Nullable final String dateCreation
    ) throws Exception;

//    @WebMethod
//    void removeProject(
//            @WebParam(name = "userId") @Nullable final String userId,
//            @WebParam(name = "id") @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllProjects(@WebParam(name = "session") @Nullable final Session session) throws Exception;

//    @WebMethod
//    void removeByNameProject(
//            @WebParam(name = "userId") @Nullable final String userId,
//            @WebParam(name = "name") @Nullable final String name) throws Exception;

    @WebMethod
    @Nullable
    List<Task> getTasksOfProjectByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name) throws Exception;

    @WebMethod
    void removeProjectAndTasksByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name) throws Exception;

    @WebMethod
    @Nullable
    List<Project> findByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name) throws Exception;

    @WebMethod
    @Nullable
    List<Project> findByDescriptionProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "description") @Nullable final String description) throws Exception;
}
