package ru.shchurin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Session;
import ru.shchurin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService(endpointInterface = "ru.shchurin.tm.endpoint.ProjectEndpoint")
public final class ProjectEndpointImpl implements ProjectEndpoint {
    @Nullable
    private ServiceLocator serviceLocator;

    public ProjectEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

//    @WebMethod
//    @NotNull
//    @Override
//    public List<Project> findAllProjects() {
//        return serviceLocator.getProjectService().findAll();
//    }

    @WebMethod
    @Nullable
    @Override
    public List<Project> findAllProjectsByUserId(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        if (!serviceLocator.getSessionService().checkSession(session)) return null;
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

//    @WebMethod
//    @NotNull
//    @Override
//    public Project findOneProject(
//            @WebParam(name = "userId") @Nullable final String userId,
//            @WebParam(name = "id") @Nullable final String id) throws Exception {
//        return serviceLocator.getProjectService().findOne(userId, id);
//    }

    @WebMethod
    @Override
    public void persistProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "dateStart") @Nullable final String dateStart,
            @WebParam(name = "dateFinish") @Nullable final String dateFinish
    ) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getProjectService().persist(name, description, dateStart, dateFinish, session.getUserId());
    }

    @WebMethod
    @Override
    public void mergeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "dateStart") @Nullable final String dateStart,
            @WebParam(name = "dateFinish") @Nullable final String dateFinish,
            @WebParam(name = "dateCreation") @Nullable final String dateCreation
    ) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getProjectService()
                .merge(id, name, description, dateStart, dateFinish, session.getUserId(), dateCreation);
    }

//    @WebMethod
//    @Override
//    public void removeProject(
//            @WebParam(name = "userId") @Nullable final String userId, @WebParam(name = "id") @Nullable final String id)
//            throws Exception {
//        serviceLocator.getProjectService().remove(userId, id);
//    }

    @WebMethod
    @Override
    public void removeAllProjects(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getProjectService().removeAll(session.getUserId());
    }

//    @WebMethod
//    public void removeByNameProject(
//            @WebParam(name = "userId") @Nullable final String userId, @WebParam(name = "name") @Nullable final String name)
//            throws Exception {
//        serviceLocator.getProjectService().removeByName(userId, name);
//    }

    @WebMethod
    @Nullable
    @Override
    public List<Task> getTasksOfProjectByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name) throws Exception {
        if (session == null) return null;
        if (!serviceLocator.getSessionService().checkSession(session)) return null;
        return serviceLocator.getProjectService().getTasksOfProject(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void removeProjectAndTasksByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getProjectService().removeProjectAndTasksByName(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    @Override
    public List<Project> findByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        if (session == null) return null;
        if (!serviceLocator.getSessionService().checkSession(session)) return null;
        return serviceLocator.getProjectService().findByName(name, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public List<Project> findByDescriptionProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "description") @Nullable final String description) throws Exception {
        if (session == null) return null;
        if (!serviceLocator.getSessionService().checkSession(session)) return null;
        return serviceLocator.getProjectService().findByDescription(description, session.getUserId());
    }
}
