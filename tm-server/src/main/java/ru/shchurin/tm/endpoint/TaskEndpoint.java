package ru.shchurin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.Session;
import ru.shchurin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.Date;
import java.util.List;

@WebService
public interface TaskEndpoint {

//    @WebMethod
//    @NotNull
//    List<Task> findAllTasks();

    @WebMethod
    @Nullable
    List<Task> findAllTasksByUserId(@WebParam(name = "session") @Nullable final Session session) throws Exception;

//    @WebMethod
//    @NotNull
//    Task findOneTask(
//            @WebParam(name = "userId") @Nullable final String userId,
//            @WebParam(name = "id") @Nullable final String id) throws Exception;

    @WebMethod
    void persistTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "dateStart") @Nullable final String dateStart,
            @WebParam(name = "dateFinish") @Nullable final String dateFinish
    ) throws Exception;

    @WebMethod
    void mergeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "dateStart") @Nullable final String dateStart,
            @WebParam(name = "dateFinish") @Nullable final String dateFinish,
            @WebParam(name = "dateCreation") @Nullable final String dateCreation
    ) throws Exception;

//    @WebMethod
//    void removeTask(
//            @WebParam(name = "userId") @Nullable final String userId,
//            @WebParam(name = "id") @Nullable String id) throws Exception;

    @WebMethod
    void removeAllTasks(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void removeByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name) throws Exception;

    @WebMethod
    @Nullable
    List<Task> findByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception;

    @WebMethod
    @Nullable
    List<Task> findByDescriptionTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "description") @Nullable final String description
    ) throws Exception;
}
