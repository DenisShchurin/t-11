package ru.shchurin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.entity.Session;
import ru.shchurin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService(endpointInterface = "ru.shchurin.tm.endpoint.TaskEndpoint")
public final class TaskEndpointImpl implements TaskEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;


    public TaskEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

//    @WebMethod
//    @NotNull
//    @Override
//    public List<Task> findAllTasks() {
//        return serviceLocator.getTaskService().findAll();
//    }

    @WebMethod
    @Nullable
    @Override
    public List<Task> findAllTasksByUserId(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        if (!serviceLocator.getSessionService().checkSession(session)) return null;
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

//    @WebMethod
//    @NotNull
//    @Override
//    public Task findOneTask(
//            @WebParam(name = "userId") @Nullable final String userId,
//            @WebParam(name = "id") @Nullable final String id) throws Exception {
//        return serviceLocator.getTaskService().findOne(userId, id);
//    }

    @WebMethod
    @Override
    public void persistTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "dateStart") @Nullable final String dateStart,
            @WebParam(name = "dateFinish") @Nullable final String dateFinish
    ) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getTaskService().persist(name, description, projectId, dateStart, dateFinish, session.getUserId());
    }

    @WebMethod
    @Override
    public void mergeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "dateStart") @Nullable final String dateStart,
            @WebParam(name = "dateFinish") @Nullable final String dateFinish,
            @WebParam(name = "dateCreation") @Nullable final String dateCreation
    ) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getTaskService()
                .merge(id, name, description, projectId, dateStart, dateFinish, session.getUserId(), dateCreation);
    }

//    @WebMethod
//    @Override
//    public void removeTask(
//            @WebParam(name = "userId") @Nullable final String userId,
//            @WebParam(name = "id") @Nullable final String id
//    ) throws Exception {
//        serviceLocator.getTaskService().remove(userId, id);
//    }

    @WebMethod
    @Override
    public void removeAllTasks(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getTaskService().removeAll(session.getUserId());
    }

    @WebMethod
    @Override
    public void removeByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getTaskService().removeByName(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    @Override
    public List<Task> findByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        if (session == null) return null;
        if (!serviceLocator.getSessionService().checkSession(session)) return null;
        return serviceLocator.getTaskService().findByName(name, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public List<Task> findByDescriptionTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "description") @Nullable final String description
    ) throws Exception {
        if (session == null) return null;
        if (!serviceLocator.getSessionService().checkSession(session)) return null;
        return serviceLocator.getTaskService().findByDescription(description, session.getUserId());
    }
}
