package ru.shchurin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.Session;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
public interface UserEndpoint {

    @WebMethod
    @Nullable
    List<User> findAllUsers(@NotNull final Session session);

//    @WebMethod
//    @Nullable
//    User findOneUser(@WebParam(name = "id") @Nullable final String id) throws Exception;

    @WebMethod
    void persistUser(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws Exception;

    @WebMethod
    void mergeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws Exception;

//    @WebMethod
//    void removeUser(@WebParam(name = "id") @Nullable final String id) throws Exception;
//
//    @WebMethod
//    void removeAllUsers();
//
//    @WebMethod
//    void removeUserByLogin(@WebParam(name = "login") @Nullable final String login) throws Exception;

//    @WebMethod
//    @NotNull
//    String getHashOfPassword(@WebParam(name = "password") @Nullable final String password) throws Exception;
//
//    @WebMethod
//    @Nullable
//    User authoriseUser(
//            @WebParam(name = "login") @Nullable final String login,
//            @WebParam(name = "password") @Nullable final String password) throws Exception;

    @WebMethod
    @Nullable
    Session getSession(@Nullable final String login, @Nullable final String password) throws Exception;

    @WebMethod
    boolean updatePassword(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "newPassword") @Nullable final String newPassword
    ) throws Exception;
}
