package ru.shchurin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.entity.Session;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.shchurin.tm.endpoint.UserEndpoint")
public final class UserEndpointImpl implements UserEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    public UserEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Nullable
    public List<User> findAllUsers(@Nullable final Session session) {
        if (session == null) return null;
        if (!serviceLocator.getSessionService().checkSession(session)) return null;
        return serviceLocator.getUserService().findAll();
    }

//    @WebMethod
//    @Nullable
//    public User findOneUser(
//            @WebParam(name = "session") @Nullable final Session session
//    ) throws Exception {
//        if (session == null) return null;
//        if (! serviceLocator.getSessionService().checkSession(session)) return null;
//        return serviceLocator.getUserService().findOne(session.getUserId());
//    }

    @WebMethod
    public void persistUser(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws Exception {
        serviceLocator.getUserService().persist(login, password);
    }

    @WebMethod
    public void mergeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws Exception {
        if (session == null) return;
        if (!serviceLocator.getSessionService().checkSession(session)) return;
        serviceLocator.getUserService().merge(session.getUserId(), login);
    }

//    @WebMethod
//    public void removeUser(@WebParam(name = "id") @Nullable final String id) throws Exception{
//        serviceLocator.getUserService().remove(id);
//    }
//
//    @WebMethod
//    public void removeAllUsers() {
//        serviceLocator.getUserService().removeAll();
//    }
//
//    @WebMethod
//    public void removeUserByLogin(@WebParam(name = "login") @Nullable final String login) throws Exception {
//        serviceLocator.getUserService().removeByLogin(login);
//    }

//    @WebMethod
//    @NotNull
//    public String getHashOfPassword(@WebParam(name = "password") @Nullable final String password) throws Exception {
//        return serviceLocator.getUserService().getHashOfPassword(password);
//    }

//    @WebMethod
//    @Nullable
//    public User authoriseUser(
//            @WebParam(name = "login") @Nullable final String login,
//            @WebParam(name = "password") @Nullable final String password) throws Exception {
//        return serviceLocator.getUserService().authoriseUser(login, password);
//    }

    @WebMethod
    @Nullable
    public Session getSession(@Nullable final String login, @Nullable final String password) throws Exception {
        return serviceLocator.getSessionService().getSession(login, password);
    }

    @WebMethod
    public boolean updatePassword(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "newPassword") @Nullable final String newPassword) throws Exception {
        if (session == null) return false;
        if (!serviceLocator.getSessionService().checkSession(session)) return false;
        return serviceLocator.getUserService().updatePassword(session, newPassword);
    }
}
