package ru.shchurin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@Getter
@Setter
public abstract class AbstractEntity {

    @Nullable
    protected String id = UUID.randomUUID().toString();
}
