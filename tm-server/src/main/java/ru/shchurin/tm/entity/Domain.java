package ru.shchurin.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Domain implements Serializable {

    private static final long serialVersionUID = 1L;

    public Domain(@NotNull final List<Project> projects, @NotNull final List<Task> tasks,
                  @NotNull final List<User> users
    ) {
        this.projects = projects;
        this.tasks = tasks;
        this.users = users;
    }

    @Nullable
    @XmlElementWrapper
    private List<Project> projects;

    @Nullable
    @XmlElementWrapper
    private List<Task> tasks;

    @Nullable
    @XmlElementWrapper
    private List<User> users;
}
