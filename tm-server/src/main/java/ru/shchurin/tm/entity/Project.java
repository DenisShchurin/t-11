package ru.shchurin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.enumerated.Status;
import ru.shchurin.tm.util.DateUtil;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @Nullable
    private String userId;

    @Nullable
    private Status status = Status.STATUS_SCHEDULED;

    @Nullable
    private Date dateCreation = new Date();

    public Project(
            @Nullable final String id, @Nullable final String name, @Nullable final String description,
            @Nullable final Date dateStart, @Nullable final Date dateFinish, @Nullable final String userId,
            @Nullable final Date dateCreation
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        this.userId = userId;
        this.dateCreation = dateCreation;
    }

    public Project(
            @Nullable final String name, @Nullable final String description, @Nullable final Date dateStart,
            @Nullable final Date dateFinish, @Nullable final String userId
    ) {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        this.userId = userId;
    }

    @NotNull
    @Override
    public String toString() {
        return "Project{" +
                ", ID='" + id + '\'' +
                "project name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateStart=" + DateUtil.dateFormat(dateStart) +
                ", dateFinish=" + DateUtil.dateFormat(dateFinish) +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", dateCreation=" + DateUtil.dateFormat(dateCreation) +
                '}';
    }
}
