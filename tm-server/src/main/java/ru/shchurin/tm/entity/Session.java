package ru.shchurin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity {

    @Nullable
    private String signature;

    @NotNull
    private Long timestamp = System.currentTimeMillis();

    @Nullable
    private String userId;

    @Nullable
    private Role role;
}
