package ru.shchurin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @Nullable
    private String projectId;

    @Nullable
    private String userId;

    @Nullable
    private Status status = Status.STATUS_SCHEDULED;

    @Nullable
    private Date dateCreation = new Date();

    public Task(
            @Nullable String id, @Nullable final String name, @Nullable final String description,
            @Nullable final String projectId, @Nullable final Date dateStart, @Nullable final Date dateFinish,
            @Nullable final String userId, @Nullable final Date dateCreation
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        this.projectId = projectId;
        this.userId = userId;
        this.dateCreation = dateCreation;
    }

    public Task(
            @Nullable final String name, @Nullable final String description, @Nullable final String projectId,
            @Nullable final Date dateStart, @Nullable final Date dateFinish, @Nullable final String userId
    ) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        this.userId = userId;
    }

    @NotNull
    @Override
    public String toString() {
        return "Task{" +
                ", ID='" + id + '\'' +
                "task name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateStart=" + dateStart +
                ", dateFinish=" + dateFinish +
                ", projectId='" + projectId + '\'' +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", dateCreation=" + dateCreation +
                '}';
    }
}
