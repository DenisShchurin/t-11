package ru.shchurin.tm.exception;

public final class ConsoleLoginException extends Exception {
    public ConsoleLoginException() {
        super("YOU ENTERED INCORRECT LOGIN");
    }
}
