package ru.shchurin.tm.exception;

public final class ConsoleNameException extends Exception {
    public ConsoleNameException() {
        super("YOU ENTERED INCORRECT NAME");
    }
}
