package ru.shchurin.tm.exception;

public final class ConsolePasswordException extends Exception {
    public ConsolePasswordException() {
        super("YOU ENTERED INCORRECT NEW PASSWORD");
    }
}
