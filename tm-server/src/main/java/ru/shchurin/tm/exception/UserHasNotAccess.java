package ru.shchurin.tm.exception;

public final class UserHasNotAccess extends Exception {
    public UserHasNotAccess() {
        super("You have not access");
    }
}
