package ru.shchurin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.TaskRepository;
import ru.shchurin.tm.entity.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class TaskRepositoryImpl extends AbstractRepository<Task> implements TaskRepository {

    @NotNull
    @Override
    public List<Task> findAll() {
        return new ArrayList<>(entities.values());
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> userTasks = new ArrayList<>();
        for (@NotNull final Task task : entities.values()) {
            if (userId.equals(task.getUserId()))
                userTasks.add(task);
        }
        return userTasks;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Task task = entities.get(id);
        if (task == null) return null;
        if (task.getUserId() == null) return null;
        if (task.getUserId().equals(userId)) return task;
        return null;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Task task = entities.get(id);
        if (task.getUserId() == null) return;
        if (task.getUserId().equals(userId))
            entities.remove(id);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        for (@NotNull final Iterator<Map.Entry<String, Task>> it = entities.entrySet().iterator(); it.hasNext(); ) {
            @NotNull final Map.Entry<String, Task> e = it.next();
            if (userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Iterator<Map.Entry<String, Task>> it = entities.entrySet().iterator(); it.hasNext(); ) {
            @NotNull final Map.Entry<String, Task> e = it.next();
            if (name.equals(e.getValue().getName()) && userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }
}
