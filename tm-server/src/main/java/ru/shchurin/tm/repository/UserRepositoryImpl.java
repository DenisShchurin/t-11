package ru.shchurin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.UserRepository;
import ru.shchurin.tm.entity.User;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class UserRepositoryImpl extends AbstractRepository<User> implements UserRepository {

    @NotNull
    @Override
    public List<User> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String id) {
        return entities.get(id);
    }

    @Override
    public void remove(@NotNull final String id) {
        entities.remove(id);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        for (@NotNull final Iterator<Map.Entry<String, User>> it = entities.entrySet().iterator(); it.hasNext(); ) {
            @NotNull final Map.Entry<String, User> e = it.next();
            if (login.equals(e.getValue().getLogin())) {
                it.remove();
            }
        }
    }

    @Override
    public boolean updatePassword(@NotNull final String userId, @NotNull final String newHashPassword) {
        @Nullable final User user = findOne(userId);
        if (user == null) return false;
        user.setHashPassword(newHashPassword);
        return true;
    }
}
