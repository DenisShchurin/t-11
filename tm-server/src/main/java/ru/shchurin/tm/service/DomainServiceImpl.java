package ru.shchurin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.DomainService;
import ru.shchurin.tm.api.ProjectService;
import ru.shchurin.tm.api.TaskService;
import ru.shchurin.tm.api.UserService;
import ru.shchurin.tm.entity.Domain;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.util.DateUtil;

import javax.xml.bind.*;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class DomainServiceImpl implements DomainService {

    @NotNull
    private final ProjectService projectService;

    @NotNull
    private final TaskService taskService;

    @NotNull
    private final UserService userService;

    private Domain getDomain() {
        @NotNull final List<Project> projects = projectService.findAll();
        @NotNull final List<Task> tasks = taskService.findAll();
        @NotNull final List<User> users = userService.findAll();
        @NotNull final Domain domain = new Domain(projects, tasks, users);
        return domain;
    }

    private void load(@NotNull final Domain domain) throws Exception {
        if (domain.getProjects() != null)
            for (@NotNull final Project project : domain.getProjects()) {
                projectService.merge(
                        project.getId(), project.getName(), project.getDescription(),
                        DateUtil.dateFormat(project.getDateStart()), DateUtil.dateFormat(project.getDateFinish()),
                        project.getUserId(), DateUtil.dateFormat(project.getDateCreation())
                );
            }

        if (domain.getTasks() != null)
            for (@NotNull final Task task : domain.getTasks()) {
                taskService.merge(
                        task.getId(), task.getName(), task.getDescription(), task.getProjectId(),
                        DateUtil.dateFormat(task.getDateStart()), DateUtil.dateFormat(task.getDateFinish()),
                        task.getUserId(), DateUtil.dateFormat(task.getDateCreation())
                );
            }

        if (domain.getUsers() != null)
            for (@NotNull final User user : domain.getUsers()) {
                userService.merge(user.getId(), user.getLogin());
            }
    }

    public DomainServiceImpl(
            @NotNull final ProjectService projectService,
            @NotNull final TaskService taskService,
            @NotNull final UserService userService) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
    }

    @Override
    public void dataSerialize() throws Exception {
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream("tm-server/src/main/data/getDomain.txt");
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        @NotNull final Domain domain = getDomain();
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
    }

    public void dataDeserialize() throws Exception{
        @NotNull final FileInputStream fileInputStream = new FileInputStream("tm-server/src/main/data/getDomain.txt");
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        load(domain);
    }

    @Override
    public void fasterXmlSaveXml() throws Exception {
        @NotNull final ObjectMapper mapper = new XmlMapper();
        @NotNull final File file = new File("tm-server/src/main/data/fasterxml.xml");
        @NotNull final Domain domain = getDomain();
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);
    }

    @Override
    public void fasterXmlLoadXml() throws Exception {
        @NotNull final ObjectMapper mapper = new XmlMapper();
        @NotNull final String xml = inputStreamToString(new FileInputStream("tm-server/src/main/data/fasterxml.xml"));
        @NotNull final Domain domain = mapper.readValue(xml, Domain.class);
        load(domain);
    }

    private String inputStreamToString(@NotNull final InputStream is) throws Exception {
        @NotNull final StringBuilder sb = new StringBuilder();
        @Nullable String line;
        @NotNull final BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }

    @Override
    public void fasterXmlSaveJson() throws Exception {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final File file = new File("tm-server/src/main/data/fasterxml.json");
        @NotNull final Domain domain = getDomain();
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);

    }

    @Override
    public void fasterXmlLoadJson() throws Exception {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final File file = new File("tm-server/src/main/data/fasterxml.json");
        @NotNull final Domain domain = mapper.readValue(file, Domain.class);
        load(domain);
    }

    @Override
    public void jaxbMarshallingXml() throws Exception {
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File("tm-server/src/main/data/jaxb.xml");
        marshaller.marshal(domain, file);
    }

    @Override
    public void jaxbUnmarshallingXml() throws Exception {
        @NotNull final File file = new File("tm-server/src/main/data/jaxb.xml");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        load(domain);
    }

    @Override
    public void jaxbMarshallingJson() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("eclipselink.media-type", "application/json");
        @NotNull final Class[] classes = new Class[]{Domain.class};
        @NotNull final JAXBContext context = JAXBContext.newInstance(classes, properties);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File("tm-server/src/main/data/jaxb.json");
        marshaller.marshal(domain, file);
    }

    @Override
    public void jaxbUnmarshallingJson() throws Exception{
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("eclipselink.media-type", "application/json");
        @NotNull final Class[] classes = new Class[]{Domain.class};
        @NotNull final File file = new File("tm-server/src/main/data/jaxb.json");
        @NotNull final JAXBContext context = JAXBContext.newInstance(classes, properties);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        load(domain);
    }
}
