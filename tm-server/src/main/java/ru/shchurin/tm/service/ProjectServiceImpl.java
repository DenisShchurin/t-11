package ru.shchurin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ProjectRepository;
import ru.shchurin.tm.api.ProjectService;
import ru.shchurin.tm.api.TaskRepository;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.exception.*;
import ru.shchurin.tm.util.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class ProjectServiceImpl implements ProjectService {

    @NotNull
    private final ProjectRepository projectRepository;

    @NotNull
    private final TaskRepository taskRepository;

    public ProjectServiceImpl(
            @NotNull final ProjectRepository projectRepository, @NotNull final TaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        return projectRepository.findAll(userId);
    }

    @NotNull
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String id) throws ConsoleIdException,
            ProjectNotFoundException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (id == null || id.isEmpty())
            throw new ConsoleIdException();
        @Nullable final Project project = projectRepository.findOne(userId, id);
        if (project == null)
            throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public void persist(
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String dateStart,
            @Nullable final String dateFinish,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty())
            return;
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        if (description == null || description.isEmpty())
            throw new ConsoleDescriptionException();
        if (dateStart == null)
            throw new ConsoleStartDateException();
        if (dateFinish == null)
            throw new ConsoleEndDateException();

        Project project = new Project(
                name, description, DateUtil.parseDate(dateStart), DateUtil.parseDate(dateFinish), userId
        );
        projectRepository.persist(project);
    }

    @Override
    public void merge(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String dateStart,
            @Nullable final String dateFinish,
            @Nullable final String userId,
            @Nullable final String dateCreation
    ) throws Exception {
        if (userId == null || userId.isEmpty())
            return;
        if (id == null || id.isEmpty())
            return;
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        if (description == null || description.isEmpty())
            throw new ConsoleDescriptionException();
        if (dateStart == null)
            throw new ConsoleStartDateException();
        if (dateFinish == null)
            throw new ConsoleEndDateException();
        if (dateCreation == null)
            throw new ConsoleEndDateException();
        Project project = new Project(
                id, name, description, DateUtil.parseDate(dateStart), DateUtil.parseDate(dateFinish),
                userId, DateUtil.parseDate(dateCreation)
        );
        projectRepository.merge(project);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws ConsoleIdException,
            UserNotAuthorized {
        if (userId == null || userId.isEmpty()) throw new UserNotAuthorized();
        if (id == null || id.isEmpty()) throw new ConsoleIdException();
        projectRepository.remove(userId, id);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        projectRepository.removeAll(userId);
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) throws ConsoleNameException,
            UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        projectRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    public List<Task> getTasksOfProject(@Nullable final String userId, @Nullable final String name)
            throws ConsoleNameException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        @NotNull final List<Task> projectTasks = new ArrayList<>();
        @NotNull final List<Project> projects = projectRepository.findAll(userId);
        @Nullable String projectId = null;
        for (@NotNull final Project project : projects) {
            if (name.equals(project.getName()))
                projectId = project.getId();
        }
        if (projectId == null) return projectTasks;

        @NotNull final List<Task> allTasks = taskRepository.findAll(userId);
        for (@NotNull final Task task : allTasks) {
            if (projectId.equals(task.getProjectId()))
                projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public void removeProjectAndTasksByName(@Nullable final String userId, @Nullable final String name)
            throws ConsoleNameException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        @NotNull final List<Project> projects = projectRepository.findAll(userId);
        String projectId = null;
        for (@NotNull final Project project : projects) {
            if (name.equals(project.getName()))
                projectId = project.getId();
        }
        if (projectId == null) return;

        @NotNull final List<Task> tasks = taskRepository.findAll(userId);
        for (@NotNull final Task task : tasks) {
            if (projectId.equals(task.getProjectId()) && task.getId() != null)
                taskRepository.remove(userId, task.getId());
        }
        projectRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    public List<Project> findByName(@Nullable final String name, @NotNull final String currentUserId)
            throws ConsoleNameException {
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        @NotNull final List<Project> allProjects = projectRepository.findAll(currentUserId);
        @NotNull final List<Project> projects = new ArrayList<>();
        for (@NotNull final Project project : allProjects) {
            if (project.getName() != null && project.getName().contains(name)) {
                projects.add(project);
            }
        }
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByDescription(@Nullable final String description, @NotNull final String currentUserId)
            throws ConsoleNameException {
        if (description == null || description.isEmpty())
            throw new ConsoleNameException();
        @NotNull final List<Project> allProjects = projectRepository.findAll(currentUserId);
        @NotNull final List<Project> projects = new ArrayList<>();
        for (@NotNull final Project project : allProjects) {
            if (project.getDescription() != null && project.getDescription().contains(description)) {
                projects.add(project);
            }
        }
        return projects;
    }
}
