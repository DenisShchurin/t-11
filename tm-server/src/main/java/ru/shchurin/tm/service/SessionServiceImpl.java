package ru.shchurin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.SessionService;
import ru.shchurin.tm.api.UserRepository;
import ru.shchurin.tm.api.UserService;
import ru.shchurin.tm.entity.Session;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.ConsoleLoginException;
import ru.shchurin.tm.exception.ConsolePasswordException;
import ru.shchurin.tm.util.HashUtil;
import ru.shchurin.tm.util.SignatureUtil;

import java.util.Collection;

public class SessionServiceImpl implements SessionService {

    @NotNull
    private final UserRepository userRepository;

    public SessionServiceImpl(@NotNull final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Nullable
    public Session getSession(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;

        @NotNull final Collection<User> users = userRepository.findAll(); // переделать, вытаскивать не всех юзеров
        @Nullable final String hashOfPassword = HashUtil.md5(password);
        @Nullable User userForCheck = null;
        for (@NotNull final User user : users) {
            if (login.equals(user.getLogin()) && hashOfPassword.equals(user.getHashPassword())) {
                userForCheck = user;
            }
        }
        @NotNull final Session session = new Session();
        session.setUserId(userForCheck.getId());
        session.setRole(userForCheck.getRole());
        @Nullable final String signature = SignatureUtil.sign(session, "salt", 10);
        session.setSignature(signature);
        return session;
    }

    public boolean checkSession(@NotNull final Session session) {
        @NotNull final String signatureForCheck = session.getSignature();
        session.setSignature(null);
        @Nullable final String signature = SignatureUtil.sign(session, "salt", 10);
        return signatureForCheck.equals(signature);
    }
}
