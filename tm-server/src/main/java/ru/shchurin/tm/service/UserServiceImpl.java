package ru.shchurin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.SessionService;
import ru.shchurin.tm.api.UserRepository;
import ru.shchurin.tm.api.UserService;
import ru.shchurin.tm.entity.Session;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.enumerated.Role;
import ru.shchurin.tm.exception.*;
import ru.shchurin.tm.util.HashUtil;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class UserServiceImpl implements UserService {

    @NotNull
    private final UserRepository userRepository;

    public UserServiceImpl(@NotNull final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        return userRepository.findOne(id);
    }

    @Override
    public void persist(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) {
            throw new ConsoleLoginException();
        }
        if (password == null || password.isEmpty()) {
            throw new ConsoleHashPasswordException();
        }
        @Nullable final String hashPassword = HashUtil.md5(password);
        User user = new User(login, hashPassword);
        userRepository.persist(user);
    }

    @Override
    public void merge(
            @Nullable final String userId, @Nullable final String login
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (login == null || login.isEmpty()) return;

        @Nullable final User user = findOne(userId);
        user.setLogin(login);
        userRepository.merge(user);
    }

    @Override
    public void remove(@Nullable final String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        userRepository.remove(id);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws ConsoleLoginException {
        if (login == null || login.isEmpty()) {
            throw new ConsoleLoginException();
        }
        userRepository.removeByLogin(login);
    }

//    @Nullable
//    @Override
//    public String getHashOfPassword(@Nullable final String password) throws Exception {
//        if (password == null || password.isEmpty()) {
//            throw new ConsolePasswordException();
//        }
//        return HashUtil.md5(password);
//    }

    @Override
    public boolean updatePassword(@Nullable final Session session, @Nullable final String newPassword) throws Exception {
        if (newPassword == null || newPassword.isEmpty())
            throw new ConsolePasswordException();
        @Nullable final String newHashPassword = HashUtil.md5(newPassword);
        if (newHashPassword == null) return false;
        return userRepository.updatePassword(session.getUserId(), newHashPassword);
    }

    @Override
    public void initUserAndAdmin() throws Exception {
        @NotNull final User user = new User("User", HashUtil.md5("User"));
        @NotNull final User admin = new User("Admin", HashUtil.md5("Admin"), Role.ROLE_ADMIN);
        userRepository.persist(user);
        userRepository.persist(admin);
    }
}
